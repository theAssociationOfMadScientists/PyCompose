from typing import Optional, Dict, List

class DeploymentModel():
    """Store information about a particular deployment."""

    default_user = {
        "name": "D. Scott Boggs",
        "email": "scott@tams.tech"
    }

    def __init__(
        self,
        user: Optional[Dict[str, str]],
        primary_contact: str,
        business: str,
        admin_password: Optional[str],
        deploy_wordpress: bool
    ):
        self.user = user if user is not None else default_user
        self.primary_contact = primary_contact
        self.business = business
        self.admin_password = admin_password
        self.deploy_wordpress = deploy_wordpress

class Contact():
    """Database model for a contact."""

    def __init__(
            self,
            first_name: Optional[str]=None,
            last_name: Optional[str]=None,
            phone_number_1: Optional[str]=None,
            phone_number_2: Optional[str]=None,
            phone_number_3: Optional[str]=None,
            email_1: Optional[str]=None,
            email_2: Optional[str]=None,
            email_3: Optional[str]=None,
            position: Optional[str]=None,
            business=None
    ):
        self.first_name = first_name
        self.last_name = last_name
        self.phone_number_1 = phone_number_1
        self.phone_number_2 = phone_number_2
        self.phone_number_3 = phone_number_3
        self.email_1 = email_1
        self.email_2 = email_2
        self.email_3 = email_3
        self.position = position
        self.business = business

    def __repr__(self):
        if self.first_name and self.last_name:
            return f"<Contact '{self.first_name} {self.last_name}'>"
        if self.first_name:
            return f"<Contact '{self.first_name}'"
        if self.last_name:
            return f"<Contact '{self.last_name}'"
        if self.email_1:
            return f"<Contact '{self.email_1}'>"
        if self.business:
            return f"<Contact at '{self.business.business_name}'>"
        return "<Contact with no name>"

class Business():
    """Database model for a business's information."""

    def __init__(
            self,
            phone_number_1: Optional[str]=None,
            phone_number_2: Optional[str]=None,
            email_1: Optional[str]=None,
            email_2: Optional[str]=None,
            address_1_st: Optional[str]=None,
            address_1_zip: Optional[str]=None,
            address_2_st: Optional[str]=None,
            address_2_zip: Optional[str]=None,
            business_name: Optional[str]=None,
            business_slogan: Optional[str]=None,
            hours_summary: Optional[str]=None,
            contacts=None
    ):
        self.phone_number_1 = phone_number_1
        self.phone_number_2 = phone_number_2
        self.email_1 = email_1
        self.email_2 = email_2
        self.address_1_st = address_1_st
        self.address_1_zip = address_1_zip
        self.address_2_st = address_2_st
        self.address_2_zip = address_2_zip
        self.business_name = business_name
        self.business_slogan = business_slogan
        self.hours_summary = hours_summary
