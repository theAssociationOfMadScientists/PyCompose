import os, random, re, requests
from textwrap import wrap, dedent
from time import sleep

def _check_for_wordlist():
    """Internal function, for random_words()

    Returns a list of words, gathered form words.txt or freebsd.org"""
    wordlist = []
    #wd = os.path.dirname(os.path.abspath(
        #inspect.getfile(inspect.currentframe())))
    try:
        with open(os.path.join("home", "scott", "Documents", "code", "site-build_automation", "words.txt"), 'r') as wordlistfile:
            for line in wordlistfile.readlines():
                wordlist.append(line.strip('\n'))
    except FileNotFoundError:
        response = requests.get("http://svnweb.freebsd.org/csrg/share/dict/words?view=co&content-type=text/plain")
        wordlist = response.text.split('\n')
        with open("words.txt", 'w') as wordlistfile:
            for line in wordlist:
                wordlistfile.writelines(line + "\n")
    return wordlist

def random_words(num, sep="_"):
    """
    Generate `num` random words, separated by `sep`.

    sep defaults to _
    """
    result = ""

    wordlist = _check_for_wordlist()
    while num > 0:
        num -= 1
        result += wordlist[random.randrange(len(wordlist))]
        if num:
            result += sep
    return result

def find(pattern, directory=None):
    """Recursively check a directory for filenames matching a pattern

    directory defaults to the current working directory
    pattern is a string representing a regular expression to check for. The
        filename must match the whole regular expression; e.g. ".*\.html" will
        match "index.html" but not "index.html.bak"
    """
    if directory is None:
        directory = os.getcwd()
    for dir, kids, files in os.walk(directory):
        for f in files:
            if re.fullmatch(pattern, str(f)):
                yield os.path.join(dir, f)

# Logging
LOG, DEBUG, WARN, ERROR, SUBPROCESS_FAILURE = range(5)
DEBUG_FLAG = True
def log(text, lvl=LOG, output=None):
    """
    Log information to stdout.

    Rewrite with file writes instead of print() calls to log to file. ERROR
    and SUBPROCESS_FAILURE log levels exit the application
    """
    formatted_text = ""
    for line in wrap(dedent(text)):
        formatted_text += line + '\n'
    if lvl == DEBUG and DEBUG_FLAG is True:
        _output_text(f"{__name__} DEBUG:  {formatted_text}", output)
    elif lvl == LOG:
        _output_text(f"{__name__} LOG:  {formatted_text}", output)
    elif lvl == WARN:
        _output_text(f"{__name__} WARN:  {formatted_text}", output)
        sleep(3)
    elif lvl == ERROR:
        _output_text(f"{__name__} ERROR:  {formatted_text}", output)
        exit(ERROR)
    elif lvl == SUBPROCESS_FAILURE:
        _output_text(f"{__name__} SUPBROCESS FAILURE:  {formatted_text}", output)
        exit(SUBPROCESS_FAILURE)
    else:
        print(f"Inappropriate level for log message ({formatted_text}) was {lvl}")
        exit()

def _output_text(text, output):
    if output:
        with open(output, 'a') as outfile:
            outfile.write(text)
    else:
        print(text)

def baseN(num,b,numerals="0123456789abcdefghijklmnopqrstuvwxyz"):
    """Convert a number to any base up to 36"""
    return ((num == 0) and numerals[0]) or (baseN(
        num // b, b, numerals).lstrip(numerals[0]) + numerals[num % b])

def num_to_alpha(num, randval=False):
    """Convert a numeral value to alphanumeric

    That is, it converts the numeral value to a base36 (10 for 0-9 and 26 for
    a-z) number, which is really only useful to create random alphanumeric
    values for passwords based on a random number.

    If 'random' is True, num isn't the literal number to be converted, it's how
    many bits of entropy to gather for a random value."""
    if randval:
        return baseN(random.getrandbits(num), 36)
    else:
        return baseN(num, 36)
