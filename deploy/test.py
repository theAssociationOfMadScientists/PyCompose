## Test Suite
import docker
import subprocess
import random
import os
import re
from textwrap import dedent
from deploy.config import num_to_alpha, msg, Config
from os.path import dirname as parent_dir_of
from pytest import raises   # for some reason, this isn't installed automatically with setup.py.
# TODO look into this further.

TEST_CONTAINER_NAME = "Test Container"
TEST_CONTAINER_PATTERN = r"Test_Container_\d*_network"
TEST_NETWORK_NAME = "Test Network"
TEST_CONTAINER_IMAGE = "hello-world:latest"
TEST_CONTAINER_IMAGE_2 = "bfirsh/reticulate-splines:latest"
TEST_CONTAINER_IMAGE_3 = "bash:latest"
TEST_VOLUME_RW = os.path.join(
    parent_dir_of(
        os.path.realpath(__name__)
    ), "WritableTestVolumeFolder"
)
TEST_VOLUME_RO = os.path.join(
    parent_dir_of(
        os.path.realpath(__name__)
    ), "ReadOnlyTestVolumeFolder"
)
TEST_VOLUME_RW_MOUNT_POINT = "/home"
TEST_VOLUME_RO_MOUNT_POINT = "/root"
TEST_VOLUMES = {
    TEST_VOLUME_RO: {
        "bind": TEST_VOLUME_RO_MOUNT_POINT,
        "mode": "ro"
    },
    TEST_VOLUME_RW: {
        "bind": TEST_VOLUME_RW_MOUNT_POINT,
        "mode": "rw"
    }
}
TEST_CONTANER_ITSELF, TEST_NETWORK_ITSELF = (None for x in range(2))

def get_test_network(alt=1):
    if TEST_NETWORK_ITSELF:
        Config.logger.debug("Network already stored: {}",
            TEST_NETWORK_ITSELF.get_name()
        )
        return TEST_NETWORK_ITSELF
    from deploy.networks import Network
    Config.logger.info(msg(f"""
        Network not yet acquired, querying Docker daemon for a network named
        {TEST_CONTAINER_NAME}_{alt}"""))
    return Network(f"{TEST_CONTAINER_NAME}_{alt}")

def get_test_container(
        name=None, image=None,volumes=None, ports=None, database=None,
        environment=None
    ):
    from deploy.containers import Container
    if not name:
        name=''
        list_of_container_names = [
            c.name for c in Config.DOCKER_CLIENT.containers.list(all=True)
        ]
        while True:
            name = "{}-{}".format(
                TEST_CONTAINER_NAME, num_to_alpha(16, True)
            )
            if name not in list_of_container_names:
                break
    Config.logger.info(msg("""
        Container not yet aquired, querying docker daemon for a container with
        the parameters:
            name:       {}\n
            image:      {}\n
            network:    {}\n
            volumes:    {}\n
            ports:      {}\n
            database:   {}\n
            environment:{}\n
        """,
        name,
        str(image or TEST_CONTAINER_IMAGE),
        get_test_network(),
        volumes,
        ports,
        database,
        environment
    ))
    return Container(
        name        = name or TEST_CONTAINER_NAME,
        image       = image or TEST_CONTAINER_IMAGE,
        network     = get_test_network(),
        volumes     = volumes,
        ports       = ports,
        database    = database,
        environment = environment
    )

class Test_Network():
    def test_set_name(self):
        tn = get_test_network()
        # check for the default value
        assert re.fullmatch(TEST_CONTAINER_PATTERN, tn.get_name()), msg("""
            When using the get_name function, test network name wasn't {},
            it was {}.""",
            TEST_CONTAINER_NAME,
            tn.get_name()
        )
        assert re.fullmatch(TEST_CONTAINER_PATTERN, tn.name), msg("""
            When checking the value itself, test network name wasn't {}, it
            was {}.""", TEST_CONTAINER_NAME, tn.name
        )
        # change it, and check again.
        tn.set_name("Network for Testing")
        assert tn.name == tn.get_name() == "Network_for_Testing_network", msg("""
            After changing the value of the network name to {}, the function
            replied with the name {} and the value itself is {}. After being
            run through the set_name function, it's value should've been:
            {}.""",
            "Network for Testing",
            tn.get_name(),
            tn.name,
            "Network_for_Testing_network"
        )

    def test_set_ipam_options(self):
        from deploy.config import Config
        from docker.types import IPAMPool, IPAMConfig
        tn = get_test_network()
        # check the default IPAM config is the right types
        assert isinstance(tn.ipam_conf, IPAMConfig),\
            msg("""
                The test network's IPAM config isn't an IPAMConfig, it's a {}
                (When checking the component directly)""",
                type(tn.ipam_conf)
            )
        assert isinstance(tn.get_ipam_options(), IPAMConfig), \
            msg("""
                The test network's IPAM config isn't an IPAMConfig, it's a {}
                (When checking via the getter function""",
                type(tn.get_ipam_options())
            )
        # this is the only value that is changed at this point
        netconf = tn.get_ipam_options()["Config"]
        assert len(netconf) == 1, msg("""
                More than one (specifically, {}) configurations were found in
                the IPAM options for this network. The code is only written to
                handle one config. I'll attempt to display the configuration:
                {}""",
                len(netconf),
                json.dumps([c for c in netconf])
            )
        subnet = netconf[0]["Subnet"]
        # and its value comes from Config.available_subnets
        assert subnet.split("/")[1] == "16", msg(
            "{} has an invalid subnet mask. Assigned IP is: {}",
            tn.get_name(), subnet
        )
        assert int(subnet.split(".")[0]) == 172\
            and 30 < int(subnet.split('.')[1]) < 255\
            and int(subnet.split('.')[2])==0\
            and subnet.split('.')[3] == "0/16", msg(
                "{} has an invalid IP assignment: {}", tn.get_name(), subnet
            )
        # Try setting invalid values
        with raises(ValueError) as e_info:
            tn.set_ipam_options("invalid")
        with raises(ValueError) as e_info:
            tn.set_ipam_options("192.168.1.1")
        netconf = tn.get_ipam_options()["Config"]
        assert len(netconf) == 1, msg("""
                {} configurations were found in the IPAM options for this
                network. The code is only written to handle one config. I'll
                attempt to display the configuration:
                {}""",
                "More than one (specifically, {})".format(
                    len(netconf)
                ) if len(netconf) != 1 else "No",
                json.dumps([c for c in netconf])
            )
        # make sure the invalid value setting didn't go through.
        assert netconf[0]["Subnet"] != "invalid"\
            and netconf[0]["Subnet"] != "192.168.1.1",\
            msg("""
                setting the IPAM options was sticky in prior tests. Invalid
                input should be rejected."""
            )
        assert netconf[0]["Subnet"] == subnet, msg("""
            The proper subnet for this network should still be {}, but it was
            instead {}. Other tests may now fail due to this.""",
            subnet,
            netconf[0]["Subnet"]
        )

    def test_set_driver(self):
        tn = get_test_network()
        # check for the correct value
        assert tn.driver == tn.get_driver() == "bridge", msg("""
            Only bridge drivers are okay. Got {} from the function and {} from
            the value itself.""",
            tn.get_driver(),
            tn.driver
        )
        # check to make sure you can't set an invalid value
        with raises(AssertionError) as e_info:
            tn.set_driver("an invalid input")
        # manually set an invalid value and check that an error is thrown when
        tn.driver = "invalid"
        # it's retrieved.
        with raises(AssertionError) as e_info:
            tn.get_driver()
        tn.delete()
        TEST_NETWORK_ITSELF = None

    def test_list_connected_containers(self):
        """Create a network with a container and make sure the container is in it."""
        tc = get_test_container(image=TEST_CONTAINER_IMAGE_2) # reticulate-splines stays up I think.
        tn = tc.get_network()
        tc.up()
        assert tc.get_container() in tn.list_connected_containers(), msg("""
                The test container ({}) was not in the list of containers
                connected to the network {}, which is {}. The test container's
                network is {}.""",
                tc.get_id(short=True),
                tn.get_id(short=True),
                [c.get_id(short=True) for c in tn.list_connected_containers()],
                tc.get_network().get_id()
        )
        tc.down()

    def test_get_network(self):
        """A test to be sure the right Network is retrieved from the daemon."""
        tn = get_test_network()
        tnfd = Config.DOCKER_CLIENT.networks.get(tn.get_id())
        assert tn.get_network() == tnfd, msg("""
                The docker network (ID {}; name {}) retrieved by passing ID {},
                was not the same as the network retrieved from the get_network
                function - whose ID is {} and name is {}.""",
                tnfd.id, tnfd.name, tn.get_id(), tn.get_network().id,
                tn.get_network().name
            )

class Test_Container():
    def test_set_name(self):
        tc = get_test_container()
        assert tc.get_readable_name() == tc.readable_name == \
            TEST_CONTAINER_NAME, msg("""
                The test container's readable name wasn't "{}", it was "{}",
                when checked with the getter function; and "{}" when checked
                via the value itself.""",
                TEST_CONTAINER_NAME, tc.get_readable_name(), tc.readable_name
            )
        assert tc.get_name() == tc.name == re.sub(
                r"\W", "", re.sub(
                    r'[ -/]',r'_', TEST_CONTAINER_NAME
                )
            ).lower(), msg("""
                The test container's name wasn't "{}", it was "{}",
                when checked with the getter function; and "{}" when checked
                via the value itself.""",
                re.sub(
                    r"\W", "", re.sub(r'[ -/]',r'_', TEST_CONTAINER_NAME)
                ),
                tc.get_name(),
                tc.name
            )
        tc.set_name("A Different Name")
        assert tc.get_readable_name() == tc.readable_name == \
            "A Different Name", msg("""
                Failed to set readable name in test container. Values:
                    tc.get_readable_name(): {}
                    tc.readable_name: {}
                should be "A Different Name".""",
                tc.get_readable_name(), tc.readable_name
            )
        assert tc.get_name() == tc.name == "a_different_name", msg("""
                Failed to set name in test container. Values:
                    get_name(): {}
                    name: {}
                should be "a_different_name".""", tc.get_name(), tc.name)
        tc.set_name(TEST_CONTAINER_NAME)    # reset back to default.

    def test_set_image(self):
        """Verify that a test container has its default image, then change it."""
        tc = get_test_container()
        im = Config.DOCKER_CLIENT.images.get(name=TEST_CONTAINER_IMAGE)
        assert tc.get_image().id == tc.image.id == im.id, msg("""
                    The image in the test container, as retrieved by the function
                    was {}, as retrieved directly as an attribute was {}. The
                    docker daemon replied with this, instead: {}. The original
                    definition of the image was: {}""",
                    tc.get_image().short_id,
                    tc.image.short_id,
                    im.short_id,
                    TEST_CONTAINER_IMAGE
                )
        tc.set_image(TEST_CONTAINER_IMAGE_2)
        im = Config.DOCKER_CLIENT.images.get(name=TEST_CONTAINER_IMAGE_2)
        assert tc.get_image().id == tc.image.id == im.id, msg("""
                After changing the image for the container to {}, the image, as
                retrieved through the function, was {}; as retrieved as an
                attribute of the test container, was {}; while the docker
                daemon responded with {}.""",
                tc.get_image().short_id,
                tc.image.short_id,
                im.short_id,
                TEST_CONTAINER_IMAGE_2
            )
        assert tc.get_image().id == Config.DOCKER_CLIENT.api.inspect_container(
                tc.get_id()
            )["Image"], msg("""
                The image retreived from the container ({}) has the ID {}, but
                when the Docker daemon was queried for the image's ID, {} was
                received. In case it's relevant, the container ({}) originally
                had the image {} with the ID {}.""",
                tc.get_image(),
                tc.get_image().short_id,
                Config.DOCKER_CLIENT.api.inspect_container(tc.get_id(short=True))["Image"],
                tc.get_readable_name(),
                Config.DOCKER_CLIENT.images.get(name=TEST_CONTAINER_IMAGE),
                Config.DOCKER_CLIENT.images.get(name=TEST_CONTAINER_IMAGE).short_id
            )

    def test_add_volumes(self):
        sonce = num_to_alpha(150, randval=True)     # A random string for this test.
        test_string = dedent(f"""
            #!/bin/bash
            echo {sonce} > $1
            """
        )
        tc = get_test_container()
        assert tc.get_volumes() == None, msg(
                "The default container should have no volumes. It has {}.",
                tc.get_volumes()
            )
        tc = get_test_container(
            image=TEST_CONTAINER_IMAGE_2,
            volumes=TEST_VOLUMES
        )
        # test get_volumes actually returns the correct results.
        assert tc.get_volumes() == TEST_VOLUMES, msg("""
                After attempting to set the volumes of a test container to {},
                the volumes retrieved from the object was: {}"""
            )
        test_file_writable = os.path.join(TEST_VOLUME_RW, "test_script.sh")
        internal_wtf = "{}/test_script.sh".format(TEST_VOLUME_RW_MOUNT_POINT)
        with open(test_file_writable, 'w') as wtf:
            wtf.write(test_string)
        tc.run("chmod 755 {}".format(internal_wtf))
        assert oct(os.stat(test_file_writable).st_mode)[-3:] == '755', msg("""
                After writing {} to {} on a volume (mounted at {} in a test
                container), and changing the permissions in the container
                to 755, the permissions were {}""",
                test_string,
                test_file_writable,
                internal_wtf,
                oct(os.stat(test_file_writable).st_mode)[-3:]
            )
        # test that the container itself received the file that was written.
        result = tc.run(f"{internal_wtf} {TEST_VOLUME_RW_MOUNT_POINT}/testfile")
        assert result == test_string, msg("""
                After writing {} to the file, and then executing
                  $ cat {}/testfile
                in the test container, the result was:
                {}""", test_string, TEST_VOLUME_RW_MOUNT_POINT, result
            )
        with raises(docker.errors.ContainerError) as e_info:
            # Test that writing to a read-only volume throws an error.
            tc.run("{internal_wtf} {TEST_VOLUME_RO_MOUNT_POINT}/testfile")

    def test_set_network(self):
        tn = get_test_network()
        tn2 = get_test_network(2)
        tc = get_test_container()
        assert tc.get_network().get_network() == tn.get_network(), msg("""
            The test container's network ({}) and the test network ({}) aren't
            equivalent.""",
            tc.get_network().get_network(short=True),
            tn.get_network(short=True)
        )
        tc.set_network(tn2)
        assert tc.get_network().get_id() == tn2.get_id(), msg("""
                After attempting to change the network of the container, the
                network the container is connected to ({}) is not {}""",
                tc.get_network().get_id(short=True), tn2.get_id(short=True)
            )
        with raises(AssertionError) as e_info:
            # check that setting an arbitrary type raises an error
            tc.set_network(57.3) # an invalid value (strs are accepted.)
        tc.set_network(tn)   # check that it works by passing the
        assert tc.get_network().get_id() == tn.get_id(), msg(
            "Setting {} network to {} container failed. Container's network is: {}",
            tn.get_id(short=True),
            tc.get_readable_name(),
            tc.get_network().get_id(short=True)
        )

    def test_get_container(self):
        tc = get_test_container()
        tcfd = Config.DOCKER_CLIENT.containers.get(tc.get_id())
        assert tc.get_container() == tcfd, msg("""
                The Container (ID {}; name {}) retrieved from the docker daemon
                by passing ID {}, was not the same as the network retrieved
                from the get_container function - whose ID is {} and name is
                {}.""",
                tcfd.id, tcfd.name, tc.get_id(), tc.get_network().id,
                tc.get_network().name
            )
