#!/usr/bin/env python3.6
#Container modules
import re
import subprocess
import os
from textwrap import wrap, dedent
from socket import inet_aton as valid_ip
from deploy.scottslibs import num_to_alpha, random_words
from deploy.config import Config, msg
import docker.models.images
from docker import DockerClient
from docker.errors import NotFound, APIError, ImageNotFound
from deploy.models import Business, Contact, DeploymentModel

class Container():
    """A base class to be extended by other containers.

    attributes of self:
      name              --  The name of the container in the docker service
      log               --  A method for logging
      container_actual  --  A docker.Container object representing the
                            container itself
      image             --  The image the container is based on
      volumes           --  A dict of volumes mounted by the container
      ports             --  A dict of ports to be exposed, where key = host port
                            and value = container port
      database          --  Either a DatabaseContainer object or False if this
                            container doesn't require a database
      network           --  The subnet to connect the container to"""
    def __init__(self, name, image, network,
            volumes=None, ports=None, database=None, environment=None):
        self.set_name(name)
        self.add_volumes(volumes)
        if self.get_volumes():
            self.check_folder()
        self.set_ports(ports)
        self.set_environment(environment)
        self.set_database(database)
        self.set_image(image)
        try:
            self.get_existing(net=network)
        except NotFound as err:
            try:
                self.create_new(net=network)
            except ImageNotFound:
                log(f"Image {self.image} not found for {self.name}.")
            except APIError:
                log(f"""
                    Container creation has failed with error message
                    {err}""")

    def __repr__(self):
        try:
            return "<deploy.containers.Container object with ID {}>".format(
                self.get_id(short=True)
            )
        except AttributeError:
            return "<deploy.containers.Container object with the name {}>".format(
                self.get_name()
            )

    def __eq__(self, value):
        if isinstance(value, Container):
            return self.get_id() == value.get_id()
        elif isinstance(value, docker.models.containers.Container):
            return self.get_id() == value.id
        return self.get_id() == value   \
            or self.get_name() == value \
            or self.get_readable_name() == value

    def get_existing(self, net):
        """Try to get the container object directly from the DockerClient.

        Uses the name attribute to find the container, so calling set_name()
        first is a must."""
        self.set_container(self.get_docker_client().containers.get(self.get_name()))
        self.get_logger().debug(f"Container {self.name} retrieved by name.\
                ({self.get_container().short_id})")
        self.set_network(net)


    def create_new(self, net):
        """Create a new container with the given attributes.

        Call if get_existing fails."""
        self.set_container(
            self.get_docker_client().containers.create(
                name=self.get_name(),
                image=self.get_image(),
                volumes=self.get_volumes(),
                ports=self.get_ports(),
                environment={ k: v for k, v in self.get_environment() }
            )
        )
        self.get_logger().info("""
            Container created with attributes:
            Name:       {0}
            Base Image: {1}
            Volumes:    {2}
            Ports:      {3}""".format(
                self.get_name(),
                self.get_image().tags[0] if len(self.get_image().tags) == 1 \
                    else self.get_image().tags,
                [ key for key in self.get_volumes().keys() ] if isinstance(
                        self.get_volumes(), dict
                    ) else "None",
                self.get_ports()
            ))
        self.set_network(net)

    def up(self):
        """Brings up this container, it's associated database (if applicable),
            and connects to the associated network."""
        self.get_logger().debug(f"Starting container {self.get_name()}")
        self.get_container().start()
        if self.get_database():
            self.get_database().up()
        self.get_network().connect(self.get_container())

    def down(self, rm=False, rmv=False):
        """Brings down this container, optionally removing it.

        Begins by disconnecting the container from its associated network, then
        calls container_actual.stop(), unless rm is True, in which case it also
        calls container_actual.remove() and sets the container_actual object to
        None."""
        self.get_network().disconnect(self.get_container())
        self.get_logger().debug(f"Stopping container {self.get_name()}")
        self.get_container().stop()
        if rm:
            self.get_logger().info(f"Removing container {self.get_name()}")
            self.get_container().remove(v=rmv)
            self.container_actual = None    # can't use a setter; validation

    def restart(self):
        """Restart the given container."""
        self.down()
        self.up()

    def run(self, command=None, rm=False, daemonize=False):
        """Start this container, run a command, and then stop it.

        If the container is already running, the command will simply be executed
        in the running container, other parameters are ignored.

        command:    The command to be passed to the container's shell. Must be a
                    list or a string according to the docker documentation. May
                    also now be None, which simply calls self.up(), to avoid
                    confusion. This means "container.run()" will start a service,
                    but "container.run('a command', rm=True)" will only run the
                    one command.
        rm:         Since this function calls down(), the rm parameter can be
                    passed on to it, causing the container to be deleted after
                    it stops.
        daemonize:  if True, the command will be run in the background, and this
                    container's interface to the docker daemon will be replaced
                    by the resulting container.

        See help(docker.models.containers) for more information on the
        underlying API calls."""
        if command is None:
            self.up()
            return
        if self.isup():
            result = self.get_container().exec_run(command)
            self.get_logger().info(msg(
                "Executing command {} in {} resulted in {}.",
                command, self.get_name(), result
            ))
            return result
        assert isinstance(command, str) or isinstance(command, list),\
            "Command to be passed to the container must be a list or a str."
        assert isinstance(rm, bool),\
            "rm must be True or False, received {} of type {}.".format(rm,
                type(rm))
        if self.get_container()\
                in self.get_docker_client().containers.list(all=True):
            self.down(rm=True, rmv=False)
        result = self.get_docker_client().containers.run(
            image=self.get_image().id,
            command=command,
            detach=daemonize,
            name=self.get_name() + "_" + re.sub("\W", "", re.sub(" ", '-', command)),
            volumes=self.get_volumes(),
            ports=self.get_ports(),
            environment={ k: v for k, v in self.get_environment() },
            auto_remove = (rm or not daemonize)
        )
        if daemonize:
            self.set_container(result)
        self.get_logger().info(
            "Executing command {} in {}. Result of execution: {}".format(
                command,
                self.get_name(),
                "Daemonized container {}.".format(
                    self.get_id(short=True)
                ) if daemonize else result
            )
        )
        return result.decode() if not daemonize else result.get_id()

    def isup(self):
        """Check to see if this is in the list of running containers."""
        return self.get_container() in Config.DOCKER_CLIENT.containers.list()

    def get_docker_client(self):
        """Return the docker client."""
        docker_client = Config.DOCKER_CLIENT
        assert isinstance(docker_client, DockerClient), \
            "Client object is not a client, it is a {}".format(type(client))
        return docker_client

    def set_network(self, network):
        """Assign this container to a Network()

        param network:  str representing the network name
            or          The network object to associate.

        Passing a network object itself is preferred, as the str form may be
        deprecated upon implementation of network drivers besides "bridge".
        """
        from deploy.networks import Network
        if isinstance(network, Network):
            self.get_logger().debug(
                "Relating container {} with network {}.".format(
                    self.get_name(),
                    network.get_name()
                )
            )
            self.network = network
            self.connect_to(network)
        else:
            assert isinstance(network, str), msg("""
                    Specified network must be a str or Network. Received
                    {} of type {}. If passing a string, the string should be
                    the name.""",
                    network,
                    type(network)
                )
            self.set_network(Network(name=network))
    def get_network(self):
        """Return the Network object associated with this Container"""
        from deploy.networks import Network
        assert isinstance(self.network, Network), \
            "Network must be a Network(). Recieved {} of type {}".format(
                self.network,
                type(self.network)
            )
        return self.network

    def check_folder(self):
        """A folder will be created for the current container for persistence.

        Folder is located at RELATIVE_ROOT/container_name/"""
        if os.access(os.path.join(Config.RELATIVE_ROOT, self.get_name()), os.W_OK):
            self.get_logger().debug(f"Folder for {self.get_name()} exists.")
            return
        self.get_logger().debug("Creating folder for {0} at {1}/{0}".format(
            self.get_name(),
            Config.RELATIVE_ROOT
        ))
        os.makedirs(os.path.join(Config.RELATIVE_ROOT, self.get_name()))

    def get_name(self):
        """Return the name of the current container object."""
        assert isinstance(self.name, str),\
            "Name is not a string it is {} of type {}".format(
                self.name,
                type(self.name)
            )
        assert not re.match(r'\W', self.name),\
            "Container name has invalid characters! {self.name}"
        return self.name

    def set_name(self, name):
        """Set the name of the current working container.

        The name given by the user is saved as a "readable name", which you can
        get with get_readable_name(). From that string, we then replace spaces,
        hyphens, and slashes with underscores, then strip anything that isn't a
        letter, number, or underscore, and save that for internal
        identification."""
        assert isinstance(name, str),\
            "Given name is not a string, it is {} of type {}. Not setting."\
                .format(name, type(name))
        self.readable_name = name
        self.name = re.sub(r"\W", "", re.sub(r'[ -/]',r'_', name)).lower()
        self.get_logger().debug(dedent(f"""
            Container name has been set to '{self.readable_name}', converted
            for internal reference to '{self.name}'"""))


    def get_readable_name(self):
        """Return the readable name of the current container object."""
        assert isinstance(self.readable_name, str),\
            "Name is not a string it is {} of type {}".format(
                self.readable_name,
                type(self.readable_name)
            )
        return self.readable_name

    def get_logger(self):
        """return the current logger method."""
        from deploy.config import Config
        return Config.logger

    def set_image(self, image):
        """Set which image this container should use."""
        if isinstance(image, str):
            try:
                self.image = self.get_docker_client().images.get(image)
                self.get_logger().debug(
                    "Image for {} set to {}, successfully.".format(
                        self.get_name(), self.image
                    )
                )
            except NotFound:
                self.get_logger().info("""
                    Image {} for container {} was not found locally.
                    Attempting to pull.""".format(
                        image, self.get_name()
                    )
                )
                if ":" in image:
                    name, tag = image.split(":", maxsplit=1)
                else:
                    name, tag = image, None
                cli = docker.APIClient(
                    base_url=Config.DOCKER_SOCKET_LOCATION,
                    version=Config.DOCKER_API_VERSION,
                    timeout=Config.IMAGE_PULL_TIMEOUT
                )
                self.get_logger().info(
                    cli.pull(
                        repository=name, tag=tag
                    )
                )
                self.image = self.get_docker_client().images.get(image)
                self.get_logger().debug(
                    "Image for {} set to {}, successfully.".format(
                        self.get_name(), self.image
                    )
                )
        elif isinstance(image, docker.models.images.Image):
            self.image = image
        else:
            raise TypeError("Recieved image is a {}.".format(type(image)))

    def set_url(self):
        """Set the URL of the current container.

        If TEST_SUBDOMAIN gloabal variable is set (it is) uses that domain
        name, otherwise, it generates one from the name of the container."""
        if Config.TEST_SUBDOMAIN:
            self.url = f"https://{Config.TEST_SUBDOMAIN}.{Config.DOMAIN_NAME}"
        else:
            self.url = f"{self.get_name()}.{Config.DOMAIN_NAME}"
        self.get_logger().debug("URL for {} set to {}".format(
            self.get_name(), self.get_url()
        ))

    def get_url(self):
        """Return the URL of the current container, generating if not exists."""
        try:
            assert isinstance(self.url, str),\
                "URL is not a string it is {self.url} of type {type(self.url)}"
            self.get_logger().debug("Returning {} for contianer {}".format(
                self.url, self.get_readable_name()
            ))
            return self.url
        except AttributeError:
            self.set_url()
            return self.get_url()
            # ^^ could introduce infinite recursive loop

    def get_image(self):
        """Return the Image object associated with this container."""
        assert isinstance(self.image, docker.models.images.Image), \
            "self.image is not a Image object it is {self.image} of type {type(self.image)}"
        return self.image

    def add_volumes(self, volumes):
        """Add a volume or volumes to be mounted by the current container."""
        if volumes is None:
            self.get_logger().debug("No volumes specified for {}".format(
                self.get_name
            ))
            self.volumes = volumes
            return
        assert isinstance(volumes, dict), \
            f"Volumes definition {volumes} must be a dict. It's a {type(volumes)}"
        try:
            for mount_point, config in volumes.items():
                assert isinstance(config, dict), dedent(f"""
                    Volume defintion for {k} must be a dict, it is {v} of
                    type {type(v)}""")
                self.volumes[mount_point] = config
                if not os.access(mount_point, os.W_OK):
                    if os.access(mount_point, os.R_OK):
                        self.get_logger().error(
                            f"{mount_point} exists but is not writable."
                        )
                    else:
                        self.get_logger().debug(
                            f"Creating folder at {mount_point}"
                        )
                        os.makedirs(mount_point)
        except AttributeError:
            self.volumes = {}
            self.add_volumes(volumes)

    def get_volumes(self):
        """Return a dict of the volumes and their configuration."""
        assert isinstance(self.volumes, dict) or self.volumes is None, \
            f"Volumes are not defined properly: {volumes}"
        return self.volumes


    def set_environment(self, environment):
        """Set the environment for this container to the dict received.

        Unconditionally replaces the current environment with the one received,
        as long as it's a valid dictionary with mappings of str to str, or
        None to erase all set environment variables."""
        if environment is None:
            self.environment = {}
            return
        assert isinstance(environment, dict), dedent(f"""
            Environment received was {environment} of type {type(environment)}.
            If you meant to add an individual variable, use add_environment().
            """)
        for k, v in environment.items():
            assert isinstance(k, str) and isinstance(environment[k], str), dedent(f"""
                Environment variable keys and values must both be strings. They
                are key: {k} of type {type(k)} and value: {v} of type
                {type(v)}.""")
        self.get_logger().info(dedent(f"""
            Setting environment for {self.get_name()} to
            {environment}"""
        ))
        self.environment = environment

    def add_environment(self, key, value):
        """Add a key-value pair to the current existing environment."""
        if value is None:
            value = ""
        assert isinstance(key, str) and isinstance(value, str), dedent(f"""
            Environment variable keys and values must both be strings. They
            are key: {key} of type {type(key)} and value: {value} of type
            {type(value)}. If you meant to set all environment variables at
            once, pass a dict to set_environment.
             """)
        try:
            self.environment[key] = value
            self.get_logger().debug(
                f"Environment variable {key} was set to {value}, in container {self.get_name()}"
            )
        except AttributeError:  # self.environment hasn't yet been defined.
            self.get_logger().debug(dedent("""
                Environment for {self.get_name()} hasn't been defined yet.
                Defining it as '{%s:%s}'"""), key, value
            )
            self.environment = { key: value }
        except TypeError:
            if self.environment is None:
                self.get_logger().debug(dedent("""
                    Environment for {self.get_name()} has been defined as
                    empty. Redefining it as '\{%s:%s\}'"""), key, value
                )
                self.environment = { key: value }
            else:
                raise


    def get_environment(self, check=False):
        """Gather the environment from the container and store it.

        Test to be sure that the values retrieved don't explicitly differ
        from the values that were set before adding any missing values to the
        global environment dict and returning it.
        """
        assert isinstance(self.environment, dict) or self.environment is None,\
            "Retreived environment was not a dict. It's {}, of type {}".format(
                self.environment, type(self.environment)
            )
        assert isinstance(check, bool),\
            "Parameter 'check' must be bool, got {}, of type {}.".format(
                check, type(check)
            )
        if check:
            try:
                env_from_container = self.get_docker_client().api.inspect_container(self.name)['Config']['Env']
                self.get_logger().debug(
                    f"Environment retrieved from container:\n{env_from_container}"
                )
            except AttributeError:
                self.get_logger().critical("Name must be specified before gathering environment from container")
            except NotFound:
                self.get_logger().warn(f"""
                    Container must be created before gathering environment from
                    container. Container name: {self.name}""")
                env_from_container = {}
            if len(env_from_container):    # (is greater than 0)
                try:
                    key, val = var.split('=', maxsplit=1)
                except ValueError:
                    self.get_logger().warn(f"Issue while trying to parse environment variable '{var}'.")
                    if var[-1:] is '=':
                        key = var.strip('=')
                        val = None
                    else:
                        raise
                self.add_environment(key, val)
                self.get_logger().debug(dedent(f"""
                    Added environment variable '{key}={value}' from container
                    to local storage of environment."""))
        for key, value in self.environment.items():
            yield key, value

    def set_database(self, db_type):
        """Set up a DatabaseContainer object from a str of the type.

        Accepts:
            "mariadb"
            "postgres"
            None
        """
        self.get_logger().debug("Setting database for {} to {}".format(
            self.get_name(), db_type
        ))
        if isinstance(db_type, DatabaseContainer):
            self.datbase = db_type
            return
        assert db_type is "mariadb"\
                or db_type is "postgres"\
                or db_type is None, dedent(f"""
            Received DB isn't a 'mariadb' or 'postgres' it is {db_type} of
            type {type(db_type)}""")
        self.database = DatabaseContainer(
            name=self.get_name(), db=db_type, network=self.get_network()
        ) if db_type else None


    def get_database(self):
        """Validate and return the DatabaseContainer object associated with
        this container.
        """
        try:
            if self.database is None:
                return None
            assert isinstance(self.database, DatabaseContainer), dedent(f"""
                Database object is not a DatabaseContainer, it is
                {self.database} of type {type(self.database)}""")
        except AttributeError:
            self.get_logger().debug("You need to set the datbase before you can get it")
            raise
        return self.database

    def set_ports(self, ports):
        """Set the ports to be forwarded from this container through the host.

        ports must be a dictionary where the keys are integers representing
        the host port and the values are the corresponding client ports."""
        try:
            assert isinstance(ports, dict), dedent(f"""
                Receieved ports are not in dict form, they are {ports} of type
                {type(ports)}""")
            for k, v in ports.items():
                assert isinstance(k, int) and isinstance(v, int), \
                    "Port values must be integers."
                self.get_logger().debug(
                    "Associating container port %d with host port %d.", k, v
                )
                self.ports[k] = v
        except AttributeError:
            self.ports = {}
            self.set_ports(ports)
        except AssertionError:
            assert ports is None, "Ports must be dict or None for no ports"
            self.ports = {}


    def get_ports(self):
        """Return the port configuration.

        If there are no ports specified the ports specification is stored as an
        empty dict internally, but this method returns None in that case,
        because that is what should be sent to the docker client for no ports,
        not an empty dict, although I imagine an empty dict would probably work
        too.
        """
        try:
            assert isinstance(self.ports, dict), f"\
                Ports are not in dict form, they are {ports} of type{type(ports)}"
        except AttributeError:
            return {}
        for k, v in self.ports.items():
            assert isinstance(k, int) and isinstance(v, int), \
                "Port values must be integers."
        return self.ports if len(self.ports) else None

    def connect_to(self, network):
        """Directly calls connect_container_to_network on the given Network.

        network must be a deploy.networks.Network object (from this package)."""
        from deploy.networks import Network
        assert isinstance(network, Network), "Given network must be a network object"
        self.get_docker_client().api.connect_container_to_network(
            container=self.get_id(),
            net_id=network.get_id()
        )
        self.get_logger().info("Container %s successfully connected to %s",
            self.get_name(),
            network.get_name()
        )

    def set_business_info(self, business: Business):
        """Retrieve the business info from the database.

        business must be a Business as defined in models.py."""
        self.business = business
        self.get_logger().debug("Business '{}' has been loaded".format(
            business.business_name
        ))

    def get_business_info(self) -> Business:
        return self.business

    def get_primary_contact(self) -> Contact:
        assert list(self.primary_contact.keys()) is \
            ["name", "email", "password"], dedent(f"""
                Stored primary contact does not have the right values.
                Received {self.primary_contact.keys()}."""
            )
        return self.primary_contact

    def set_primary_contact_info(self, primary_contact: Contact):
        """Retrieve the business info from the database."""
        if len(primary_contact.first_name) == 0:
            primary_contact.first_name = "admin"
        elif len(primary_contact.first_name) <= 3:
            primary_contact.first_name += "_admin"
        self.primary_contact = primary_contact
        self.get_logger().debug(
            "Primary contact for {} container set to '{} {}'".format(
                self.get_business_info().business_name,
                primary_contact.first_name,
                primary_contact.last_name
            )
        )

    def set_admin_password(self, admin_password):
        """Store the admin password for the current deployment."""
        assert isinstance(admin_password, str), dedent("""
            Admin password must be a string, received {} of type {}""".format(
                admin_password, type(admin_password)
            ))
        self.admin_password = admin_password

    def get_admin_password(self):
        """Retrieve the admin password for the current deployment."""
        assert isinstance(self.admin_password, str),\
            "Admin password must be a string, received {} of type {}".format(
                self.admin_password, type(self.admin_password)
            )
        return self.admin_password

    def get_id(self, short=False):
        """Retreive the hash ID of the container"""
        return self.get_container().short_id if short else self.get_container().id

    def get_container(self):
        """Retrieve the interface to the docker client for the given container.

        Returns a docker.models.containers.Container object."""
        if self.container_actual is None:
            try:
                self.get_existing()
            except docker.errors.NotFound:
                self.create_new()
        assert isinstance(self.container_actual,
            docker.models.containers.Container), dedent("""
                Retrieved container was not a docker API container, it was
                {} of type {}.""".format(
                    self.container_actual, type(self.container_actual)
                )
            )
        return self.container_actual

    def set_container(self, container):
        """Store an interface to the docker API for this container.

        container must be a docker.models.containers.Container."""
        assert isinstance(container,
            docker.models.containers.Container), dedent("""
                Retrieved container was not a docker API container, it was {},
                of type {}""".format(
                    container, type(container)
                )
            )
        self.container_actual = container

class DatabaseContainer(Container):
    """An instance of a MariaDB or PostGreSQL container."""
    def __init__(self, name, db, network, environment=None, volumes=None,
            ports=None):
        """Create an instance of a DockerContainer.

        The MRO is very important in this case as some functions depend on
        values set by others.
        """
        self.set_db_type(db)
        self.set_name(name)    # must come after set_db_type
        self.set_database(None) # Meaning this container doesn't require a
        # database back-end. Because it is a database.
        self.set_network(network)
        self.set_image("{}:latest".format(self.get_db_type())) # the rest of
        self.set_ports(ports)   # these log referring to the name to help track
        self.check_folder()     # down issues, so must come after set_name()
        self.add_volumes(volumes)
        self.set_environment(environment)

        try:
            self.get_existing()
        except NotFound:
            self.create_new()

    def set_db_type(self, db):
        """Validate and set the type of database.

        Currently accepts 'mariadb' and 'postgres', but 'postgres' will raise
        a NotImplementedError down the line, as it's not yet implemented.
        """
        assert isinstance(db, str) and (db == "mariadb" or db == "postgres"),\
            dedent(f"""
                database specification must be 'mariadb' or 'posgres'. Received:\
                {db}\
                of type {type(db)}""")
        self.database_type = db

    def get_db_type(self):
        """return the type of database"""
        assert self.database_type is "mariadb" or self.database_type is\
            "postgres", dedent("""
                Database type must be 'mariadb' or 'postgres'. Found {} of
                type {}""".format(
                    self.database_type, type(self.database_type)
                )
            )
        if self.database_type is "postgres":
            raise NotImplementedError("PostGreSQL is not yet implemented.")
        return self.database_type

    def get_db_port(self):
        """Return a string representing the port number of the database type."""
        if self.get_db_type() is "mariadb":
            return "3306"
        elif self.get_db_type() is "postgres":
            return "5678"
        else:
            raise NotImplementedError

    def set_environment(self, environment=None):
        """Set the environment for the current container.

        Overrides the parent object's set_environment to set some default
        values before calling the superfunction."""
        name = self.get_name()
        self.database_name      = f"{name}_db"
        self.database_user      = f"{name}_db_user"
        self.pw_file            = os.path.join(Config.RELATIVE_ROOT, name,
                                               f"{self.name}.pw.env")
        if self.get_db_type() is "mariadb":
            default_env = {
                "MYSQL_DATABASE": self.get_name(),
                "MYSQL_USER": self.get_name(),
                "MYSQL_PASSWORD": self.get_database_password(),
                "MYSQL_RANDOM_ROOT_PASSWORD": "true"
            }
            import json
            def ppdict(obj):
                return json.dumps(obj)
            self.get_logger().debug(dedent("""
                Default environment prepared for {}:
                {}{}""".format(
                    name, ppdict(default_env),
                    f"\nAbout to add environment:\n{environment}" if environment\
                        else ''
                )
            ))
        elif self.get_db_type() is "postgres":
            raise NotImplementedError(self.get_db_type())
        else:
            raise NotImplementedError(self.get_db_type())
        assert isinstance(environment, dict) or environment is None, \
            dedent(f"""
                To specifiy environment values, pass the environment as a dict.
                Received {environment}""")
        if environment is not None:
            for key, value in environment.items():
                default_env[key] = value
        super().set_environment(default_env)

    def get_database_password(self):
        """Generate a password if not found in the appropriate file."""
        if os.access(self.pw_file, os.R_OK):
            self.get_logger().debug("Reading DB password from file.")
            with open(self.pw_file, 'r') as pwfile:
                return pwfile.read().split('=')[1]
        else:
            self.get_logger().debug("Database password not found, generating new one.")
            try:
                database_password = num_to_alpha(
                    Config.ENTROPY_BITS,
                    randval=True
                )
                with open(self.pw_file, 'w') as pwfile:
                    pwfile.write(f"MYSQL_PASSWORD={database_password}")
                return database_password
            except PermissionError as e:
                self.get_logger().error(dedent(f"""
                    While trying to write {self.pw_file}, got permision denied:
                    {e.args}"""))

    def add_volumes(self, volumes=None):
        """Set volume mounts for this databse container.

        Set the default volume configuration for the type of databse specified,
        allowing them to be overridden with the volumes parameter.

        Accepts:
          volumes:
            a dict mapping a host path to a dict specifying configuration
            options required by the docker API. See
            help(docker_client.containers.run) for more information."""
        if self.get_db_type() is "mariadb":
            default_vols = {
                os.path.join(Config.RELATIVE_ROOT, self.get_name(), "database"): {
                    "bind": "/var/log/mysql",
                    "mode": "rw"
                }
            }
        elif self.get_db_type() is "postgres":
            # TODO
            raise NotImplementedError(self.get_db_type())
        else:
            raise NotImplementedError(self.get_db_type())
        if isinstance(volumes, dict):
            for k, v in volumes.items():
                default_vols[k] = v
        super().add_volumes(default_vols)

    def set_name(self, name):
        super().set_name(f"{name} {self.get_db_type()}")

class WordpressContainer(Container):
    """A container to define a Wordpress application.

    Set up a database container and some default values before calling
    super.__init__() to create the the actual Container object."""
    def __init__(self, deployment, network):
        self.set_admin_password(deployment.admin_password)
        self.set_business_info(deployment.business)
        self.set_name(self.get_business_info().business_name)
        self.set_primary_contact_info(deployment.primary_contact)
        self.set_network(network)
        self.set_image(f"wordpress:{Config.WORDPRESS_VERSION}-php{Config.PHP_VERSION}-apache")
        self.set_database("mariadb")
        self.add_volumes({
            os.path.join(Config.RELATIVE_ROOT, self.get_name(), "webroot"): {
                'bind': "/var/www/html",
                'mode': "rw"
            }
        })
        self.set_environment()
        self.set_url()
        deployment.set_wordpress_url(self.get_url())
        try:
            self.get_existing()
        except docker.errors.NotFound:
            self.create_new()
            self.do_installation()

    def set_environment(self):
        """Initialize the default necessary environment variables."""
        db_env = { k: v for k, v in self.get_database().get_environment() }
        super().set_environment({
            "VIRTUAL_HOST":         self.get_url(),
            "DEFAULT_HOST":         self.get_url(),
            "LETSENCRYPT_HOST":     self.get_url(),
            "LETSENCRYPT_EMAIL":    Config.SYSADMIN_EMAIL,
            "WORDPRESS_DB_NAME":    db_env["MYSQL_DATABASE"],
            "WORDPRESS_DB_USER":    db_env["MYSQL_USER"],
            "WORDPRESS_DB_PASSWORD":db_env["MYSQL_PASSWORD"],
            "WORDPRESS_DB_HOST":    "{}:{}".format(
               self.get_database().get_name(),
               self.get_database().get_db_port()
            )
        })

    def up(self):
        """Bring up the database, then itself."""
        self.get_database().up()
        super().up()
        if not os.access(
                os.path.join(
                    Config.RELATIVE_ROOT, self.get_name(), "webroot", "wp-config.php"
                ), os.R_OK ):
            self.do_installation()

    def do_installation(self):
        """Get a WP-CLI container and run the necessary installation scripts."""
        cli = self.get_wp_cli()
        cli.run("wp core download")
        environ = { k: v for k, v in self.get_environment() }
        cli.run(
            "wp core config --dbhost={} --dbname={} --dbuser={} --dbpass={}").format(
                self.get_database().get_name(),
                environ["WORDPRESS_DB_NAME"],
                environ["WORDPRESS_DB_USER"],
                environ["WORDPRESS_DB_PASSWORD"]
            )
        self.run("chmod 644 wp-config.php")
        cli.run(dedent("""
            wp core install --url={} --title="{}" --admin_name={}
             --admin_password={} --admin_email={}""".format(
                self.get_url(),
                self.get_readable_name(),
                self.get_primary_contact().first_name,
                self.get_admin_password(),
                Config.SYSADMIN_EMAIL)
            ), rm=True
        )

    def get_wp_cli(self):
        """Create a wp-cli container.

        The returned Container() has access to this container's volumes,
        network, and environment.

        One should use the "run" function of the returned object to perform
        any commands, passing rm=True to the final one. See do_installation
        for an example."""
        self.get_logger().debug(
            "Creating wp-cli container for {}".format(
                self.get_name()
            )
        )
        return Container(
            name        = "{}-cli".format(self.get_name()),
            image       = "wordpress:cli",
            volumes     = self.get_volumes(),
            network     = self.get_network(),
            environment = { k: v for k, v in self.get_environment() }
        )

    def set_name(self, name):
        """Append "_wordpress" before setting"""
        super().set_name(f"{name} WordPress")

class NextCloudContainer():
    """"""
    def __init__(self, deployment_id, network):
        deployment = Deployment.query.get(deployment_id)
        self.set_admin_password(deployment.admin_password)
        self.set_business_info(deployment.business.identifier)
        self.set_name(self.get_business_info().business_name)
        self.set_network(network)
        self.set_primary_contact_info(deployment.primary_contact.identifier)
        self.set_database("mariadb")
        self.set_image(f"nextcloud:{Config.NEXTCLOUD_VERSION}-apache")
        self.add_volumes({
            os.path.join(Config.RELATIVE_ROOT, self.get_name(), "webroot"): {
                'bind'  : "/var/www/html",
                'mode'  : "rw"
            }
        })
        self.set_environment()
        super().__init__(
            name           = self.get_name(),
            image          = self.get_image(),
            database       = self.get_database(),
            volumes        = self.get_volumes(),
            environment    = { k: v for k, v in self.get_environment() },
        )
        self.do_installation()

    def set_environment(self):
        """Initialize the default environment variables."""
        super().set_environment({
            "VIRTUAL_HOST":         self.get_url(),
            "DEFAULT_HOST":         self.get_url(),
            "LETSENCRYPT_HOST":     self.get_url(),
            "LETSENCRYPT_EMAIL":    Config.SYSADMIN_EMAIL,
        })
    def do_installation(self):
        """Use the 'occ' command in the container to install NextCloud."""
        pass
