"""Objects for working with groups of services as Container()s."""
import docker
import os
import re
import stat
import json
from deploy.containers import Container, DatabaseContainer, WordpressContainer
from textwrap import wrap, dedent
from deploy.models import DeploymentModel
from deploy.config import Config

CONTAINER_TYPES = (Container, DatabaseContainer, WordpressContainer)

class Network():
    """An interface to a particular docker network."""
    def __init__(self, name, driver="bridge"):
        self.set_name(name)
        self.set_driver(driver)
        try:
            self.get_existing()
        except docker.errors.NotFound:
            self.create_new()

    def __repr__(self):
        try:
            return "< deploy.networks.Network object with ID {} >".format(
                self.get_id(short=True)
            )
        except AttributeError:
            return "< Uninitialized deploy.networks.Network object >"

    def __eq__(self, value):
        """Return whether self == value.

        If value is a docker.models.networks.Network deploy.networks.Network
        it will check the against self.get_id(). Otherwise, it simply checks if
        value == self.get_id(), so value should be a string network SHA ID."""
        if isinstance(value, Network):
            return self.get_id() == value.get_id()
        elif isinstance(value, docker.models.networks.Network):
            return self.get_id() == value.id
        return self.get_id() == value

    def get_existing(self):
        """Get an existing network from the docker interface by name."""
        self.set_network(self.get_docker_client().networks.get(self.get_name()))
        self.get_logger().debug(
            dedent("""
                Network {} retrieved by name. ({})""".format(
                    self.name, self.get_network().short_id
                )
            )
        )
        self.set_ipam_options(
            subnet=Config.DOCKER_CLIENT.api.inspect_network(
                self.get_id()
            )["IPAM"]["Config"][0]["Subnet"]
        )
        self.get_logger().debug("Current network config is:\n{}".format(
            json.dumps(
                Config.DOCKER_CLIENT.api.inspect_network(self.get_id()),
                indent=2
            )
        ))

    def set_ipam_options(self, subnet):
        """Set up IP options.

        subnet is not a SubNet (sorry for the confusion, I may revise that),
        it's an actual subnet, like the networking term. It represents a block
        of IP addresses available to this Network.

        Other IPAM options should be added to this, TODO."""
        self.get_logger().debug("Setting IPAM options with subnet={}".format(
            subnet
        ))
        IP_pattern = r"172\.\d?\d?\d?\.0\.0/\d?\d?"
        if not re.fullmatch(IP_pattern, subnet):
            raise ValueError(subnet)
        self.ipam_pool = docker.types.IPAMPool(subnet=subnet)
        self.ipam_conf = docker.types.IPAMConfig(
            pool_configs=[self.ipam_pool]
        )

    def get_ipam_options(self):
        """Retrieve the networking options for this network."""
        assert isinstance(self.ipam_conf, docker.types.IPAMConfig),\
            "Retrieved IP configuration isn't IPAMConfig it's {} of type {}.".format(
                self.ipam_conf, type(self.ipam_conf)
            )
        return self.ipam_conf

    def create_new(self):
        """Create a new network based on the configuration options.

        See help(docker_client.networks.create) for more information."""
        errcount = 0
        while True:
            try:
                self.set_ipam_options(subnet=Config.available_subnets.pop())
                self.get_logger().info(dedent("""
                    Attempting to create new Docker network with options:
                    Name:               {}
                    Driver:             {}
                    IP range/subnet:    {}""".format(
                        self.get_name(),
                        self.get_driver(),
                        self.get_ipam_options()[0]["Subnet"] if
                            len(self.get_ipam_options()) <= 1 else
                            self.get_ipam_options()
                    )
                ))
                self.set_network(
                    self.get_docker_client().networks.create(
                        name    = self.get_name(),
                        driver  = self.get_driver(),
                        ipam    = self.get_ipam_options()
                    )
                )
                self.get_logger().debug("Network creation succeeded")
                break
            except docker.errors.APIError as err:
                if err.status_code == 404:
                    raise
                if errcount > 5:
                    raise
                self.get_logger().warn(dedent(f"""
                    Error from docker client follows. IP range is likely taken,
                    will try again.
                    {err.args}
                    {err.strerror}"""
                ))
                errcount += 1
            except IndexError:
                self.get_logger().critical("Out of IPs!")
                exit()

    def get_docker_client(self):
        """Return the docker interaction object."""
        docker_client = Config.DOCKER_CLIENT
        assert isinstance(docker_client, docker.DockerClient), \
            "Client object is not a client, it is a {}".format(
                type(docker_client)
            )
        return docker_client

    def set_driver(self, driver):
        """Set which network driver to use for this network.

        driver should be a string representing the type of network to use.
        See https://docs.docker.com/engine/userguide/networking for more
        information. At the current stage, the only compatible network
        driver is "bridge", so basically it has to be that."""
        assert driver is "bridge", \
            f"Currently, only bridge networks are supported. Received {driver}."
        self.driver = driver

    def get_driver(self):
        """Return the driver to be used for this network."""
        assert self.driver is "bridge", "Currently, only bridge networks are supported. Currently set driver is {driver}."
        return self.driver

    def get_logger(self):
        """Retrieve the logger function."""
        return Config.logger

    def get_name(self):
        """Return the name of the current network."""
        assert isinstance(self.name, str),\
            "Name is not a string it is {} of type {}".format(
                self.name,
                type(self.name)
            )
        return self.name

    def set_name(self, name):
        """Set the name of the current network."""
        # todo check for validity of name
        if ' ' in name:
            self.name = "{}_network".format(name.replace(' ', '_'))
        else:
            self.name = f"{name}_network"

    def get_id(self, short=False):
        """Retrieve the ID of the current network.

        If short is truthy, retrieve the short ID (just the first 8 or
        10 characters) instead."""
        return self.network_actual.short_id if short else self.network_actual.id

    def set_network(self, network):
        """Check type then store the Docker network as an attr."""
        assert isinstance(network, docker.models.networks.Network), \
            "Received network isn't a Network it's {} of type {}".format(
                network, type(network)
            )
        self.network_actual = network

    def get_network(self):
        """Retrieve the object for interacting with the actual docker Network"""
        assert isinstance(self.network_actual, docker.models.networks.Network), \
            "Received network isn't a Network it's {} of type {}".format(
                self.network_actual, type(self.network_actual)
            )
        return self.network_actual

    def list_connected_containers(self):
        return self.get_network().containers

    def connect(self, container):
        """Connect the given container to this network.

        The container must be either
        <class 'docker.models.containers.Container'> or the locally defined
        deploy.containers.Container."""
        if isinstance(container, docker.models.containers.Container):
            # act upon the container
            try:
                self.get_network().connect(
                    container,
                    aliases=[container.name, container.id]
                )
            except docker.errors.APIError as err:
                # TODO more complete error checking
                if err.status_code == 403:
                    self.get_logger().error(f"Error from the docker client:\n\
                        (probably already connected) \n\
                        {err.args}")
                else:
                    raise
        elif isinstance(container, Container):
            self.connect(container.get_container())
        else:
            # wrong type received, reaching this represents a bug.
            raise TypeError(
                "Tried to connect {} (of type {}) to a network.".format(
                    container, type(container)
                )
            )

    def disconnect(self, container):
        """Disconnect the given container from this network, if connected."""
        if isinstance(container, docker.models.containers.Container):
            # act upon the container
            if container in self.list_connected_containers():
                self.get_logger().debug(dedent("""
                    Disconnecting container {} from network {}""".format(
                        container.name, self.get_name()
                    )
                ))
                self.get_network().disconnect(container)
            else:
                self.get_logger().warn(dedent("""
                    Attempted to disconnect container {} from network {}, which
                    it is not currently connected to.""".format(
                        container.name, self.get_name()
                    )
                ))
                return False
        elif isinstance(container, Container):
            self.disconnect(container.get_container())
        else:
            # wrong type received, reaching this represents a bug.
            raise TypeError(
                "Tried to connect {} (of type {}) to a network.".format(
                    container, type(container)
                )
            )

    def delete(self):
        """Disconnect and stop all containers and delete the network."""
        for container in self.list_connected_containers():
            container.down()
            self.disconnect(container)
        Config.DOCKER_CLIENT.networks.get(
                network_id=self.get_id()
            ).remove()

class SubNet():
    """An object to hold a group of services that are networked.

    Every SubNet creates a network, a reverse proxy, its associated
    LetsEncrypt companion, and the requested services as a docker network and
    containers, respectively.
    """
    def __init__(self, name, services=None):
        assert isinstance(name, str), \
            f"Given name must be a string. Received {name} of type {type(name)}"
        self.set_name(name)
        if services:
            assert hasattr(services, "__iter__"), dedent(f"""
                Services must be an iterable of Container objects. Received
                {services} of type {type(services)}""")
            for service in services:
                assert isinstance(service, CONTAINER_TYPES), dedent(f"""
                    Services must be an iterable of Container objects. Received
                    {service} of type {type(service)} at position
                    {services.index(service)} in {services} of type
                    {type(services)}""")
                self.add_service(service)
            self.up()
        else:
            self.create()

    def __repr__(self):
        return "<deploy.networks.SubNet Object With {} Services>".format(
            len((svc for svc in self.get_services()))
        )

    # TODO: __eq__

    def get_docker_client(self):
        docker_client = Config.DOCKER_CLIENT
        assert isinstance(docker_client, docker.DockerClient), dedent(f"""
            In get_docker_client, Docker client was not DockerClient object,
            it's {client} of type {type(client)}""")
        return docker_client

    def get_docker_client_socket_location(self):
        docker_socket_file_location = Config.DOCKER_SOCKET_FILE_LOCATION
        assert isinstance(docker_socket_file_location, str), dedent(f"""
            In get_docker_client_socket_location, docker_socket_file_location
            was not a string, it was {docker_socket_file_location} of type
            {type(docker_socket_file_location)}""")
        return docker_socket_file_location

    def get_logger(self):
        return Config.logger

    def set_name(self, name):
        """Set the name of the current working SubNet.

        The name given by the user is saved as a "readable name", which you can
        get with get_readable_name(). From that string, we then replace spaces,
        hyphens, and slashes with underscores, then strip anything that isn't a
        letter, number, or underscore, and save that for internal
        identification."""
        assert isinstance(name, str),\
            "Given name is not a string, it is {} of type {}. Not setting."\
                .format(name, type(name))
        self.readable_name = name
        self.name = re.sub(r"\W","", name.replace(r'[ -/]',r'_'))
        self.get_logger().debug(dedent(f"""
            SubNet name has been set to '{self.readable_name}', converted for
            internal reference to '{self.name}'"""))

    def get_name(self):
        """Return the name of the current container object."""
        assert isinstance(self.name, str),\
            "Name is not a string it is {} of type {}".format(
                self.name,
                type(self.name)
            )
        return self.name

    def get_readable_name(self):
        """Return the readable name of the current container object."""
        assert isinstance(self.readable_name, str),\
            "Name is not a string it is {} of type {}".format(
                self.readable_name,
                type(self.readable_name)
            )
        return self.readable_name

    def set_network(self, network):
        assert isinstance(network, Network), dedent("""
            In set_network, network must be a Network object. Received {}
            of type {}""".format(network, type(network))
        )
        self.network = network
        self.get_logger().debug(dedent(f"""
            Successfully associated SubNet {self.get_name()} with network
            {self.get_network().get_name()}"""))

    def get_network(self):
        assert isinstance(self.network, Network), dedent("""
            In set_network, network must be a Network object.
            Retrieved {} of type {}""".format(
                self.network, type(self.network)
            )
        )
        return self.network

    def add_service(self, service):
        assert isinstance(service, CONTAINER_TYPES), dedent(f"""
            Given service must be a Container object. Received {service}
            of type {type(service)}""")
        try:
            self.services.append(service)
            self.get_logger().debug(dedent(f"""
                Successfully added {service} to
                '{self.get_readable_name()}'"""
            ))
        except AttributeError:  # The first service to be added.
            self.services = [service,]
            self.get_logger().debug(dedent(f"""
                Service {service} is the first service to be
                added to '{self.get_name()}'"""
            ))
        service.set_network(self.get_network())

    def get_services(self):
        """ Return a generator of Container objects representing
        the services associated with this subnet"""
        for service in self.services:
            assert isinstance(service, CONTAINER_TYPES), dedent(f"""
                Given service must be a Container. Received {service} of type
                {type(service)}""")
            yield service

    def up(self):
        """Bring up all services managed by this subnet."""
        for svc in self.get_services():
            self.get_logger().info("Bringing up container {}.".format(
                svc.get_name()
            ))
            svc.up()

    def down(self, rm=False):
        """Bring down all services managed by this subnet."""
        for svc in self.get_services():
            if rm:
                self.get_logger().warn(
                    "Bringing down container {}, and removing it.".format(
                        svc.get_name()
                    )
                )
            else:
                self.get_logger().info(
                    "Bringing down container {},but not removing it.".format(
                        svc.get_name()
                    )
                )
            svc.down(rm)

    def restart(self):
        """Restart all services managed by this subnet."""
        for svc in self.get_services():
            self.get_logger().info("Restarting container {}.".format(
                svc.get_name()
            ))
            svc.restart()

class RevProxNet(SubNet):
    """A SubNet with overridden constructor for reverse proxy."""
    def __init__(self):
        """Create a "SubNet" with nginx reverse proxy and
        LetsEncrypt services. Any publicly facing services
        should retrieve this's network and connect to it."""
        self.set_name("reverse_proxy")
        self.set_network(Network(
            name    = self.get_name(),
        ))
        self.nginx = nginx = Container(
            network = self.get_network(),
            name    = "reverse_proxy",
            image   = "jwilder/nginx-proxy",
            volumes={
                self.get_docker_client_socket_location(): {
                    'bind': '/tmp/docker.sock',
                    'mode': 'ro'
                },
                os.path.join(Config.RELATIVE_ROOT,
                             self.get_name(),
                             "nginx-proxy",
                             "webroot"): {
                    'bind': '/usr/share/nginx/html',
                    'mode': 'rw'
                },
                os.path.join(Config.RELATIVE_ROOT,
                             self.get_name(),
                             "nginx-proxy",
                             "config"): {
                    'bind': '/etc/nginx',
                    'mode': 'rw'
                }
            },
            ports={
                80:     80,
                443:    443
            }
        )
        self.letsencrypt_companion = letsencrypt = Container(
            name        = "{}_letsencrypt_companion".format(self.get_name()),
            image       = "jrcs/letsencrypt-nginx-proxy-companion",
            network     = self.get_network(),
            volumes     = {
                self.get_docker_client_socket_location(): {
                    'bind': "/var/run/docker.sock",
                    'mode': "ro"
                },
                os.path.join(Config.RELATIVE_ROOT,
                             self.get_name(),
                             "nginx-proxy",
                             "config",
                             "certs"): {
                    'bind': '/etc/nginx/certs',
                    'mode': 'rw'
                }
            },
            environment = {
                "ACME_CA_URI": "https://acme-staging.api.letsencrypt.org/directory"
            } if Config.DEBUG_FLAG else None
        )
        for svc in (nginx, letsencrypt):
            self.add_service(svc)
        self.up()


class Deployment(SubNet):
    def __init__(self, database_entry):
        assert isinstance(database_entry, DeploymentModel), dedent(f"""
            Received database model must be a Deployment, it's {database_entry}
            of type {type(database_entry)}.""")
        from deploy.models import Business
        self.set_name(Business.query.get(database_entry.business).business_name)
        self.set_network(database_entry.identifier)
        if database_entry.deploy_wordpress:
            self.get_logger().debug("Creating wordpress container")
            wordpress = WordpressContainer(
                database_entry.identifier,
                self.get_network()
            )
            self.add_service(wordpress)

        self.connect_deployment()
        self.up()

    def up(self):
        try:
            self.get_logger().debug(f"Deploying {self.services}")
        except AttributeError:
            self.get_logger().error("No services to deploy!")
        super().up()

    def connect_deployment(self):
        for service in self.get_services():
            service.connect_to(self.get_network())

    def set_network(self, deployment_id):
        super().set_network(
            Network(name=self.get_name()+str(deployment_id))
        )
