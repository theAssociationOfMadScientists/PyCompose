from setuptools import setup

setup(
    name="Internal Deployment Script",
    version='0.0.1',
    packages=[
        "deploy"
    ],
    install_requires=[
        "flask",
        "flask_wtf",
        "flask_sqlalchemy",
        "flask_login",
        "docker",
        "flask_migrate",
        "PyYAML"
    ],
    test_requires=[
        "pytest"
    ]
)
